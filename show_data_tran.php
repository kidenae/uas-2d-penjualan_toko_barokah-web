<?php
    $db_name = "penjualan";
    $db_user = "root";
    $db_pass = "";
    $db_server_loc = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($db_server_loc,$db_user,$db_pass,$db_name);
        $sql = "select idQR,nama_brg,harga
                    from laporan";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_brg = array();
            while($brg = mysqli_fetch_assoc($result)){
                array_push($data_brg,$brg);

            }
            echo json_encode($data_brg);

        }
    }
?>